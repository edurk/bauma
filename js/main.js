
var config = {
    Inits: function(height,width){
      $('#cbp-fwslider').css('margin-left', width);  
    },
    //FIRST SECTION ANIMATE
    slide0: function(object, position, scrollLast) {
        var header = $('.header');
        var nav = $('.nav');
        var logoImg = $('.logoImg');
        var logo = $('.logo');
        
         if (header.attr('data-modified') != 'true') {
            //VALID

            header.removeClass('fixed').addClass('absolute');
            header.attr('data-modified', 'true');
            nav.css('font-weight', '100');
            logoImg.fadeOut(200);
        }
  

        var NowScroll = $(window).scrollTop();
        var currentMargin = $(object).css('-webkit-transform');
        var style = $(object);

        if (navigator.userAgent.search("Firefox") >= 0) {
            var matrix = style.css('-moz-transform');
            matrix = matrix.split('(');
            matrix = matrix[1].split(',');
            matrix = matrix[5].split(')');
            matrix = matrix[0];
            currentMargin = matrix;

        } else {
            var matrix = new WebKitCSSMatrix(style.css('-webkit-transform'));
            currentMargin = matrix.m42;

        }
        var parallax = $(object).attr('data-kmxo');
        if (parallax == 'true') {
            var upen = (scrollLast / 10) * 8;

            var opacity = (scrollLast / 1000);
            var opacity2 = opacity;
            opacity = 1 - opacity;
            $(object).css('-webkit-transform', "translate(0px, " + upen + "px)");
            $(object).css('-moz-transform', "translate(0px, " + upen + "px)");
            //$(object).css('background-position', "0px" + upen + "px");

            //$(object).stop().css('background-position', "0px " +upen+"px");
            $('.logoHead').css('opacity', opacity);
        }

    },
    //SECOND SECTION ANIMATE
    slide1: function(object, position, scrollLast) {
        var header = $('.header');
        var nav = $('.nav');
        var logoImg = $('.logoImg');
        var logo = $('.logo');
        console.log('BLABLA');
        if (header.attr('data-modified') == 'true') {
            //VALID
            
            header.removeClass('absolute').addClass('fixed');
            header.attr('data-modified', 'false');
            nav.css('font-weight', '100');
            logoImg.fadeIn(200);
        }
    },
    //FOUR SECTION ANIMATE
    slide3: function(object, position, scrollLast) {
       var offsetCharget = $(object).attr('data-offsetCharged');
       if(offsetCharget != 'true'){
      $('#cbp-fwslider').animate({'margin-left': '0px'}, 'slow');
  }

    },
    //FIVE SECTION ANIMATE
    slide4: function(object, position, scrollLast) {
        var mapActive = $('#ek-map').data('map');
        if (mapActive != 'true') {
            config.googleMap();
        }

    },
            googleMap: function() {
                var height2 = $('#lapTop4').height();
                var height = height2 / 2;
                var minheight = height2 / 2.5;
                var mapActive = $('#ek-map').data('map');
                var mp = height - 40;
                $('.ek-contact').css('height',height2);
                 //$('#ek-map').css('bottom','-'+height+'px');
                $('#ek-map').animate({'height': height}, 'fast', 'easeInQuad', function() {
                    setTimeout(function() {
                        var mapOptions = {
                            zoom: 18,
                            center: new google.maps.LatLng(41.91345832334452, 1.6814353207626027),
                            panControl: false,
                            zoomControl: true,
                            mapTypeControl: true,
                            scaleControl: false,
                            streetViewControl: false,
                            overviewMapControl: false,
                            mapTypeId: google.maps.MapTypeId.ROAD
                        };

                        var map = new google.maps.Map(document.getElementById('ek-map'), mapOptions);

                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(41.91343037964176, 1.6811831931152028),
                            map: map,
                            title: 'El Menut de la Bauma'
                        });
                        $('#ek-map').data('map', 'true');
                    }, 1000);
                });

            },
            slide0Offset: function(object, position, scrollLast){
            },
             slide1Offset: function(object, position, scrollLast){
               
            },
             slide2Offset: function(object, position, scrollLast){
                $('#cbp-fwslider').animate({'margin-left': '0px'}, 'slow');
            },
             slide3Offset: function(object, position, scrollLast){
                console.log('OFFSET3');
            },
             slide4Offset: function(object, position, scrollLast){
                
            },
            mail: function(){
                $('.ek-mail').animate({'bottom': '50%'}, 'slow');
                $('.ek-rotate').animate({'bottom': '-2207px'}, 'slow',function(){
                    $('.ek-map').animate({'opacity': '0'}, 'slow');
            });
        }
            

};

