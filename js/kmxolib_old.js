var documentHeight,
        documentWidth,
        userHeight,
        largeLap,
        mediumLap,
        shortLap,
        thirdLap,
        slides,
        current = 0,
        marginY = -10,
        scrollInit,
        deslien,
        currentFactor = 0,
        sizeArray = [],
        body;


var eduScript = {
    preinit: function() {
        eduScript.cache();
        eduScript.getsize();
        scrollInit = $(window).scrollTop();
    },
    cache: function() {
        largeLap = $('.ek-lapLarge');
        mediumLap = $('.ek-lapMedium');
        shortLap = $('.ek-lapShort');
        thirdLap = $('.ek-lapThird');
        slides = $('section');
        body = $('body');


    },
    getsize: function() {
        sizeArray = new Array();
        userHeight = $(window).outerHeight(true);
        documentHeight = $(document).height();
        documentWidth = $(document).width();
        var largeHeight = userHeight;
        var mediumHeight = parseInt(userHeight) / 2;
        var shortHeight = parseInt(userHeight) / 4;
        var thirdHeight = parseInt(userHeight) / 1.333;

        //$('.grid').css('height', largeHeight);
        thirdLap.css('height', thirdHeight);
        largeLap.css('height', largeHeight);
        mediumLap.css('height', mediumHeight);
        shortLap.css('height', shortHeight);
        
        slides.each(function(e) {
            console.log(this);
            var currentPosition = $(this).position();
            console.log(currentPosition);
            var valParallax = eduScript.validateParallax(this);
            currentPosition = currentPosition.top;
            console.log('TOP'+currentPosition);
            currentPosition = parseInt(currentPosition) - parseInt(valParallax);
            var slideClass = $(this).attr('class');
            switch (slideClass) {
                case 'ek-lapLarge' :
                    var factor = 1;
                    break;
                case 'ek-lapMedium' :
                    var factor = 0.5;
                    break;
                case 'ek-lapShort' :
                    var factor = 0.25;
                    break;
                case 'ek-lapThird' :
                    var factor = 0.75;
                    break;
            }
            //MIDA REAL DEL SECTION
            var realSize = userHeight * factor;

            //CACHE DE MIDAS
            currentFactor = parseInt(currentFactor) + parseInt(realSize);

            //CACHE DE MIDAS
           
            
            sizeArray.push(currentPosition);
            console.log(sizeArray);

        });


    },
    parallax: function() {
        //$(window).unbind('scroll.Main');
        var scrollLast = $(window).scrollTop();
        if (scrollLast > scrollInit) {
            scrollInit = scrollLast;
        } else {
            if (scrollLast < scrollInit) {
                scrollInit = scrollLast;
            } else {
                if (scrollLast == scrollInit) {
                    return false;
                }
            }
        }
        var lenghtArray = sizeArray.length;
        var i = 0;
        for (i = 0; i <= lenghtArray; i++) {
            var x = i + 1;
            //console.log(scrollLast);
            //console.log(sizeArray[i]);
            //console.log(sizeArray[x]);
            if (scrollLast >= sizeArray[i] && scrollLast < sizeArray[x]) {
                var section = $('#lapTop' + i);
                eduScript.animations(section, i, scrollLast);
                return false;
            }

        }
    },
    animations: function(object, position, scrollLast) {
        switch (position) {
            case 0 :
                config.slideOne(object, position, scrollLast);
                break;
            case 1 :
                config.slideTwo(object, position, scrollLast);
                break;
        }

    },
    goto: function(object) {
        var method = 'out';
        eduScript.uimenu(method);
        var position = $(object).attr('data-step');
        $("html, body").animate({scrollTop: sizeArray[position]}, 1000);
        var page = $(object).attr('data-text');
        var page = page.toLowerCase();
        body.attr('data-page', page);
        //history.pushState(null, null, page);
    },
    getScroll: function() {
        var NowScroll = $(window).scrollTop();
        return NowScroll;
    },
    uimenu: function(method) {
        // HACER UN EACH QUE RECORRA I RECOJA LOS VALORES DE LOS LI
        var menu = $('.menu');
        var menuUI = $('#menu-ui');
        var ui = $('.ui-menu');
        var nav = $('.nav');
        if (method == 'enter') {
            ui.css('height', userHeight);
            ui.css('background', 'rgba(0,0,0,0.3)');
            setTimeout(function() {
                menuUI.fadeIn(200);
            }, 200);
            $('.ui-menu').hover(function() {
            }, function() {
                var method = 'out';
                //eduScript.uimenu(method);
            });

            ui.unbind('click.uimenu').on('click.uimenu', '.icon-ui', function() {
                var method = 'out';
                eduScript.uimenu(method);
            });

        }
        if (method == 'out') {

            ui.css('height', '100px');
            setTimeout(function() {

                ui.css('background', 'rgba(0,0,0,0)');
            }, 200);
            ui.children('ul').fadeOut(200);
            setTimeout(function() {
                ui.children('ul').fadeOut(200);
                //ui.css('height', '100%');
            }, 200);


            $('.ui-menu').unbind('click.uimenu').on('click.uimenu', '.icon-ui', function() {
                var method = 'enter';
                eduScript.uimenu(method);
            });

        }
    },
    initPage: function() {
        var page = body.attr('data-page');
        page = page.toLowerCase();
        $('.nav').children('.menu').children('li').each(function(event) {
            var target = $(this).text();
            target = target.toLowerCase();
            if (target == page) {

                eduScript.goto(this);

            }
        });
    },
    validateParallax: function(object) {
        if ($(object).attr('data-kmxo') == 'true') {
            var style = $(object);
            if (navigator.userAgent.search("Firefox") >= 0) {
                var matrix = style.css('-moz-transform');
                matrix = matrix.split('(');
                matrix = matrix[1].split(',');
                matrix = matrix[5].split(')');
                matrix = matrix[0];
                currentMargin = matrix;

            } else {
                var currentMargin = $(object).css('-webkit-transform');
                var matrix = new WebKitCSSMatrix(style.css('-webkit-transform'));
                currentMargin = matrix.m42;
            }
        } else {
            currentMargin = 0;
        }
        return currentMargin;
    }
};
$(window).load(function() {
    $("html, body").animate({scrollTop: '1'}, 0.1);
    $("html, body").animate({scrollTop: '0'}, 0.2);
});

$(document).ready(function() {
    $("html").niceScroll({cursorcolor: "#444", cursoropacitymax: 0.7, boxzoom: true, cursorwidth: "10px", cursorborder: 0, mousescrollstep: 100});
    eduScript.preinit();
    setTimeout(function() {
        //eduScript.initPage();
    }, 100);
    $(window).resize(function() {
        eduScript.getsize();
    });
    $(window).bind('scroll.Main', function() {
        eduScript.parallax();
    });
    $('.menu').on('click', 'li', function() {
        eduScript.goto(this);
    });
    $('.ui-menu').unbind('click.uimenu').on('click.uimenu', '.icon-ui', function() {
        var method = 'enter';
        eduScript.uimenu(method);
    });



});


