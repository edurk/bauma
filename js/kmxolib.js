var documentHeight,
        documentWidth,
        userHeight,
        largeLap,
        thirdLap,
        mediumLap,
        shortLap,
        lastLap,
        slides,
        current = 0,
        marginY = -10,
        scrollInit,
        deslien,
        currentFactor = 0,
        sizeArray = [],
        body,
        fail;


var eduScript = {
    preinit: function() {
        eduScript.cache();
        eduScript.getsize();

        scrollInit = $(window).scrollTop();


    },
    cache: function() {
        largeLap = $('.ek-lapLarge');
        thirdLap = $('.ek-lapThird');
        mediumLap = $('.ek-lapMedium');
        shortLap = $('.ek-lapShort');
        lastLap = $('.ek-lapLast');
        slides = $('section');
        body = $('body');


    },
    getsize: function() {
        sizeArray = new Array();
        failHeight = 0;
        userHeight = $(window).outerHeight(true);
        documentHeight = $(document).height();
        documentWidth = $(document).width();
        config.Inits(documentHeight, documentWidth);

        var largeHeight = userHeight;
        var thirdHeight = parseInt(userHeight) / 1.33;
        var mediumHeight = parseInt(userHeight) / 2;
        var shortHeight = parseInt(userHeight) / 4;
        var lastHeight = 40;

        $('.grid').css('height', largeHeight);
        largeLap.css('height', largeHeight);
        thirdLap.css('height', thirdHeight);
        mediumLap.css('height', mediumHeight);
        shortLap.css('height', shortHeight);
        lastLap.css('height', lastHeight);
        //ONLY GOOGLEMAPS
        config.googleMap();

        slides.each(function(e) {
            var currentPosition = $(this).position();
            var topP = $(this).position();
            var valParallax = eduScript.validateParallax(this);
            if (valParallax != null) {
                currentPosition = currentPosition.top;
                var topP = topP.top;
                currentPosition = parseInt(currentPosition) - parseInt(valParallax);
                //console.log('CURRENTP:' + currentPosition);
                var slideClass = $(this).attr('class');
                switch (slideClass) {
                    case 'ek-lapLarge' :
                        var factor = 1;
                        break;
                    case 'ek-lapMedium' :
                        var factor = 0.5;
                        break;
                    case 'ek-lapShort' :
                        var factor = 0.25;
                        break;
                    case 'ek-lapThird' :
                        var factor = 0.75;
                        break;
                    default:
                        var factor = 'false';

                }
                //console.log('FACTOR:' + factor)
                //MIDA REAL DEL SECTION
                if (factor == 'false') {
                    var realSize = $(this).height();
                } else {
                    var realSize = userHeight * factor;
                }
               // console.log('UserHeight:' + userHeight);
                //console.log('RealSize:' + realSize);
                //SI FALLA JQUERY TOP POSITION
                if (e == 0) {
                    if (topP > 1) {
                        fail = 'done';
                    }
                }
                //CACHE DE MIDAS
                currentFactor = parseInt(currentFactor) + parseInt(realSize);

                //CACHE DE MIDAS
                if (fail == 'done') {
                    sizeArray.push(failHeight);
                } else {
                    sizeArray.push(currentPosition);
                }

                //SI FALLA JQUERY TOP POSITION
                if (fail == 'done') {
                    //console.log('entry3');
                    failHeight = failHeight + realSize;
                }
                //console.log('FailHeight:' + failHeight);
            }
        });

       // console.log(sizeArray);
        eduScript.margins();
        eduScript.gallery();
    },
    parallax: function() {
        //$(window).unbind('scroll.Main');
        var scrollLast = $(window).scrollTop();
        if (scrollLast > scrollInit) {
            scrollInit = scrollLast;
        } else {
            if (scrollLast < scrollInit) {
                scrollInit = scrollLast;
            } else {
                if (scrollLast == scrollInit) {
                    return false;
                }
            }
        }

        var lenghtArray = sizeArray.length;
        //console.log('lenghtArray' + lenghtArray);
        var i = 0;
        for (i = 0; i <= lenghtArray; i++) {
            var x = i + 1;
            //console.log('i' + i);
            //console.log('x' + x);
            //console.log(scrollLast + '>=' + sizeArray[i] + '&&' + scrollLast + '<' + sizeArray[x]);
            var distance = parseInt(sizeArray[x]) - parseInt(sizeArray[i]);
            var offset = parseInt(distance) / 2;
            var heightOffset = parseInt(sizeArray[x]) - parseInt(offset);
            //OFFSET ANIMATIONS
            if (scrollLast >= sizeArray[i] && scrollLast < heightOffset) {
                var section = $('#lapTop' + x);
                var offsetActive = $('#lapTop' + i).attr('data-offset');
                if (offsetActive == 'true') {
                    var offsetCharged = $('#lapTop' + i).attr('data-offsetCharged');
                    if (offsetCharged != 'true') {
                        eduScript.animationsOffset(section, i, scrollLast);
                        $('#lapTop' + i).attr('data-offsetCharged', 'true');
                    }
                }
            }
            //console.log('SCROLLLAST:' + scrollLast);
            //console.log('ARRAY-I:' + sizeArray[i]);
            //console.log('ARRAY-X:' + sizeArray[x]);

            //ANIMATIONS IN HEIGHT
            if (scrollLast >= sizeArray[i] && scrollLast < sizeArray[x]) {
                //console.log($('#lapTop' + i));
                var section = $('#lapTop' + i);
                eduScript.animations(section, i, scrollLast);
                return false;
            }


        }
    },
    animations: function(object, position, scrollLast) {
        //console.log(position);
        switch (position) {
            case 0 :
                config.slide0(object, position, scrollLast);
                break;
            case 1 :
                config.slide1(object, position, scrollLast);
                break;
            case 3 :
                config.slide3(object, position, scrollLast);
                break;
            case 4 :
                config.slide4(object, position, scrollLast);
                break;
        }

    },
    animationsOffset: function(object, position, scrollLast) {
        //console.log(position);
        switch (position) {
            case 0 :
                config.slide0Offset(object, position, scrollLast);
                break;
            case 1 :
                config.slide1Offset(object, position, scrollLast);
                break;
            case 2 :
                config.slide2Offset(object, position, scrollLast);
                break;
            case 3 :
                config.slide3Offset(object, position, scrollLast);
                break;
            case 4 :
                config.slide4Offset(object, position, scrollLast);
                break;
        }

    },
    goto: function(object) {
        var method = 'out';
        eduScript.uimenu(method);
        var position = $(object).attr('data-step');
        //console.log(position);
        $("html, body").animate({scrollTop: sizeArray[position]}, 1000);
        var page = $(object).attr('data-text');
        var page = page.toLowerCase();
        body.attr('data-page', page);
        //history.pushState(null, null, page);
    },
    getScroll: function() {
        var NowScroll = $(window).scrollTop();
        return NowScroll;
    },
    uimenu: function(method) {
        // HACER UN EACH QUE RECORRA I RECOJA LOS VALORES DE LOS LI
        var menu = $('.menu');
        var menuUI = $('#menu-ui');
        var ui = $('.ui-menu');
        var nav = $('.nav');
        if (method == 'enter') {
            ui.css('height', userHeight);
            ui.css('background', 'rgba(0,0,0,0.4)');
            setTimeout(function() {
                menuUI.fadeIn(200);
            }, 200);
            $('.ui-menu').hover(function() {
            }, function() {
                var method = 'out';
                //eduScript.uimenu(method);
            });

            ui.unbind('click.uimenu').on('click.uimenu', '.icon-ui', function() {
                var method = 'out';
                eduScript.uimenu(method);
            });

        }
        if (method == 'out') {

            ui.css('height', '100px');
            setTimeout(function() {

                ui.css('background', 'rgba(0,0,0,0)');
            }, 200);
            ui.children('ul').fadeOut(200);
            setTimeout(function() {
                ui.children('ul').fadeOut(200);
                //ui.css('height', '100%');
            }, 200);


            $('.ui-menu').unbind('click.uimenu').on('click.uimenu', '.icon-ui', function() {
                var method = 'enter';
                eduScript.uimenu(method);
            });

        }
    },
    initPage: function() {
        var page = body.attr('data-page');
        page = page.toLowerCase();
        $('.nav').children('.menu').children('li').each(function(event) {
            var target = $(this).attr('data-text');
            target = target.toLowerCase();
            if (target == page) {

                eduScript.goto(this);

            }
        });
    },
    validateParallax: function(object) {
        if ($(object).attr('data-kmxo') == 'true') {
            var style = $(object);
            if (navigator.userAgent.search("Firefox") >= 0) {
                var matrix = style.css('-moz-transform');
                matrix = matrix.split('(');
                matrix = matrix[1].split(',');
                matrix = matrix[5].split(')');
                matrix = matrix[0];
                currentMargin = matrix;
                //console.log(currentMargin);

            } else {
                var currentMargin = $(object).css('-webkit-transform');
                var matrix = new WebKitCSSMatrix(style.css('-webkit-transform'));
                currentMargin = matrix.m42;

            }

        } else {
            currentMargin = 0;
        }
        return currentMargin;
    },
    margins: function(object) {
        var height = $('#lapTop0').height();
        var marginTop = parseInt(height) / 4;
        $('.header').css('top', height + 'px');
        $('#ed-article0').css('margin-top', marginTop);
    },
    gallery: function() {
        var image = $('.ek-block5').first();
        image = image.attr('data-image');
        var text = $('.ek-block5').first().children('span').text();
        var target = $('#ek-output');
        target.css('background-image', 'url("' + image + '")');
        $('#ek-output-desc').children('span').text(text);

        $('.ek-block5').each(function() {
            image = $(this).attr('data-image');
            $(this).css('background-image', 'url("' + image + '")');
        });

        /////////////ARROWS/////////////////

        $('.ek-arrow').unbind('click.arrow').bind('click.arrow', function() {
            var step = $(this).attr('data-step');
            if (step == 0) {
                $('.ek-block2').children('p').fadeOut(200);
                $('.ek-block2').animate({'width': '5px'}, 'slow');
                $(this).animate({'left': '15px'}, 'fast');
                $(this).attr('data-step', 1);
            } else {
                $(this).animate({'left': '348px'}, 'fast');
                $('.ek-block2').animate({'width': '338px'}, 'slow');
                $('.ek-block2').children('p').delay(500).fadeIn(600);
                $(this).attr('data-step', 0);
            }
        });

        /////////////IMAGES/////////////////
        $('.ek-block5').unbind('click.change').bind('click.change', function() {
            var image = $(this).attr('data-image');
            var text = $(this).children('span').text();
            var target = $('#ek-output');
            target.css('background-image', 'url("' + image + '")');
            $('#ek-output-desc').children('span').empty().text(text);
            $('.ek-block5').each(function(){
                var clase = $(this).attr('class');
                if(clase == 'ek-block5 ek-selected'){
                    $(this).removeClass('ek-selected');
                }
            });
            $(this).addClass('ek-selected');
        });
    }
};
$(window).load(function() {
    $("html, body").animate({scrollTop: '1'}, 0.1);
    $("html, body").animate({scrollTop: '0'}, 0.2);
});

$(document).ready(function() {
    $("html").niceScroll({cursorcolor: "#444", cursoropacitymax: 0.7, boxzoom: true, cursorwidth: "10px", cursorborder: 0, mousescrollstep: 100});
    eduScript.preinit();
    setTimeout(function() {
        eduScript.initPage();
    }, 100);
    $(window).resize(function() {
        eduScript.getsize();
    });
    $(window).bind('scroll.Main', function() {
        eduScript.parallax();
    });
    $('.menu').on('click', 'li', function() {
        eduScript.goto(this);
    });
    $('.ui-menu').unbind('click.uimenu').on('click.uimenu', '.icon-ui', function() {
        var method = 'enter';
        eduScript.uimenu(method);
    });

    $('#cbp-fwslider').cbpFWSlider();
    $('.ek-mail').unbind('click.mail').bind('click.mail', function() {
        config.mail();
    });
    $('#button-blue').click(function(){
        console.log('hola');
          $.ajax({
                type: "POST",
                url: "mail.php",
                dataType: "json",
                data: $('#form1').serialize(),
                beforeSend: function() {
                    $('#button-blue').attr('disabled', 'disabled');
                    $('#button-blue').attr('disabled', 'disabled');
                    var tick = '<div class="tick"><img src="http://www.exportcontrol2014.com/images/ajax-loader.gif" width="30"></div>';
                    $('#form1').append(tick);
                },
                error: function(xref, response, hola) {
                    console.log(xref);
                },
                success: function(response) {
                    if (response.result == 'correct') {
                
                        var msg = 'Missatge enviat!';
                        var tick = '<div class="tick"><img src="http://www.exportcontrol2014.com/images/tick.png" width="30">' + msg + '</div>';
                        var form = $('#form1').children('.tick');
                        $(form).empty().append(tick);
                        $('#button-blue').removeAttr('disabled');
                        $('#button-blue').removeAttr('disabled');

                        setTimeout(function() {
                            $(form).fadeOut(400).remove();
                        }, 3000);
                    }
                }
            });
    });

});


