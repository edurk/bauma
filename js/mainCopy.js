
var config = {
    Inits: function(height,width){
      $('#cbp-fwslider').css('margin-left', width);  
    },

    slide0: function(object, position, scrollLast) {
        var header = $('.header');
        var nav = $('.nav');
        var logoImg = $('.logoImg');
        var logo = $('.logo');
        if (header.attr('data-modified') == 'true') {

            header.removeClass('solid').addClass('gradient');
            header.css('border-bottom', '1px solid rgb(255, 255, 255)');
            //nav.animate({'margin-top': '36px'}, 'fast', 'easeInQuad');
            nav.find('li').css('border-color', '#9E9E9E');
            nav.css('font-weight', 'bold');
            logoImg.fadeOut(200);
            logo.fadeOut(200);


            header.attr('data-modified', 'false');
        }

        var NowScroll = $(window).scrollTop();
        var currentMargin = $(object).css('-webkit-transform');
        var style = $(object);

        if (navigator.userAgent.search("Firefox") >= 0) {
            var matrix = style.css('-moz-transform');
            matrix = matrix.split('(');
            matrix = matrix[1].split(',');
            matrix = matrix[5].split(')');
            matrix = matrix[0];
            currentMargin = matrix;

        } else {
            var matrix = new WebKitCSSMatrix(style.css('-webkit-transform'));
            currentMargin = matrix.m42;

        }
        var parallax = $(object).attr('data-kmxo');
        if (parallax == 'true') {
            var upen = (scrollLast / 10) * 8;

            var opacity = (scrollLast / 1000);
            var opacity2 = opacity;
            opacity = 1 - opacity;
            $(object).css('-webkit-transform', "translate(0px, " + upen + "px)");
            $(object).css('-moz-transform', "translate(0px, " + upen + "px)");
            //$(object).css('background-position', "0px" + upen + "px");

            //$(object).stop().css('background-position', "0px " +upen+"px");
            //$(object).css('opacity', opacity);
        }

    },
    slide1: function(object, position, scrollLast) {
        var header = $('.header');
        var nav = $('.nav');
        var logoImg = $('.logoImg');
        var logo = $('.logo');
        if (header.attr('data-modified') != 'true') {
            //VALID

            header.removeClass('gradient').addClass('solid');
            header.css('border-bottom', '1px solid rgb(148, 97, 97)');
            //nav.animate({'margin-top': '30px'}, 'fast', 'easeInQuad');
            header.attr('data-modified', 'true');
            nav.css('font-weight', '100');
            logoImg.fadeIn(200);
            /*header.css('border', '0');
             $('.icon-ui').css('margin-top', '18px');
             header.attr('data-modified', 'true');*/

            //
            /*nav.animate({'margin-top': '0px'}, 'fast', 'easeInQuad');
             logoImg.children('img').css('margin-top', '25px');
             logoImg.children('img').css('width', '70px');
             logo.css('font-size', '140%');
             logo.css('padding-top', '25px');*/
            //nav.find('ul').css('margin-top','35px');
            //nav.find('li').css('border-color', '#414141');

        }
    },
    slide3: function(object, position, scrollLast) {
       var offsetCharget = $(object).attr('data-offsetCharged');
       if(offsetCharget != 'true'){
      $('#cbp-fwslider').animate({'margin-left': '0px'}, 'slow');
  }

    },
    slide4: function(object, position, scrollLast) {
        var mapActive = $('#ek-map').data('map');
        if (mapActive != 'true') {
            config.googleMap();
        }

    },
            googleMap: function() {
                var height = $('#lapTop4').height();
                var mapActive = $('#ek-map').data('map');
                $('#ek-map').animate({'height': height}, 'fast', 'easeInQuad', function() {
                    setTimeout(function() {
                        var mapOptions = {
                            zoom: 18,
                            center: new google.maps.LatLng(41.91345832334452, 1.6814353207626027),
                            panControl: false,
                            zoomControl: true,
                            mapTypeControl: true,
                            scaleControl: false,
                            streetViewControl: false,
                            overviewMapControl: false,
                            mapTypeId: google.maps.MapTypeId.ROAD
                        };

                        var map = new google.maps.Map(document.getElementById('ek-map'), mapOptions);

                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(41.91343037964176, 1.6811831931152028),
                            map: map,
                            title: 'El Menut de la Bauma'
                        });
                        $('#ek-map').data('map', 'true');
                    }, 1000);
                });

            },
            slide0Offset: function(object, position, scrollLast){
               
            },
             slide1Offset: function(object, position, scrollLast){
               
            },
             slide2Offset: function(object, position, scrollLast){
                $('#cbp-fwslider').animate({'margin-left': '0px'}, 'slow');
            },
             slide3Offset: function(object, position, scrollLast){
                console.log('OFFSET3');
            },
             slide4Offset: function(object, position, scrollLast){
                
            },
            mail: function(){
                $('.ek-mail').animate({'bottom': '50%'}, 'slow');
                $('.ek-rotate').animate({'bottom': '-2207px'}, 'slow',function(){
                    $('.ek-map').animate({'opacity': '0'}, 'slow');
            });
        }
            

};

