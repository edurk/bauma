<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content="Eduard Camacho Works">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>El Menut de la Bauma</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js" ></script>
        <script type="text/javascript" src="js/kmxolib.js" ></script>
        <script type="text/javascript" src="js/main.js" ></script>
        <script type="text/javascript" src="js/mobile.js" ></script>
        <script type="text/javascript" src="js/jquery.nicescroll.js" ></script>
        <script src="js/modernizr.custom.js"></script>
        <script src="js/jquery.cbpFWSlider.min.js"></script>


        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Alice' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Forum' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="css/default.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script>



        </script>

    </head>
    <?PHP
    if (isset($_GET['id']))
        $page = $_GET['id'];
    else
        $page = 'inici';
    ?>
    <body data-page="<?PHP echo $page; ?>">
    <main>
        <div class="ui-menu"><div class="icon-ui"></div>
            <ul class="menu" id="menu-ui">
                <li  data-step="0" data-text="inici">Inici</li>
                <li  data-step="1" data-text="serveis">Serveis</li>
                <li  data-step="2" data-text="serveis">Història</li>
                <li  data-step="3" data-text="galeria">Galería</li>
                <li  data-step="4" data-text="contacte">Contacte</li>
            </ul>

        </div>

        <div class="header absolute" data-modified="true">
            <div class="nav">
                <div class="logoImg"><img src="images/logocircle2.png" width="100" alt="logo"/></div>
                <div class="logo"><span>El Menut de la Bauma</span></div>
                <ul class="menu">
                    <li  data-step="0" data-text="inici">Inici</li>
                    <li  data-step="1" data-text="serveis">Serveis</li>
                    <li  data-step="2" data-text="serveis">Història</li>
                    <li  data-step="3" data-text="galeria">Galería</li>
                    <li  data-step="4" data-text="contacte">Contacte</li>
                </ul>
            </div>
        </div>

        <div class="grid"></div>
        <section class="ek-lapThird" id="lapTop0" data-step="0" data-kmxo="true" data-offset='false' >

            <div class="logoHead"><img src="images/logo2.png" alt="" style="max-width: 100%;"></div>
            <div class="logoText">Més de 80 anys fent gaudir el teu paladar</div>
        </section>

        <section class="ek-lapTwo" id="lapTop1" data-step="1" data-kmxo="false" data-offset='false'>

            <div class="ek-grid1" >
                <div class="ek-block">
                    <div class="ek-block-img floatLeft">
                        <img src="images/plates/calamar.jpg" alt="" class="img_inner">
                        <img src="images/plates/shadow.png" alt="" class="img_shadow">  
                    </div>
                    <div class="ek-block-text floatLeft">
                        <h2>Aperitius</h2>
                        <p>Ens agrada conservar la les tradicions, per això en El Menut de la Bauma pots seguir gaudint dels nostres aperitius selectes i bermuts</p>
                    </div>
                </div>
                <div class="ek-block">
                    <div class="ek-block-img floatRight">
                        <img src="images/plates/buti.jpg" alt="" class="img_inner">
                        <img src="images/plates/shadow.png" alt="" class="img_shadow">  
                    </div>
                    <div class="ek-block-text floatRight">
                        <h2>Carta</h2>
                        <p>La cuina del Menut de la Bauma, catalana i mediterrània per excel·lència, està basada en l’ús de productes naturals, de temporada i en l’encert de combinar la gran varietat del seus components. Oferim una gran varetat de plats casolans per a escollir, peixos, mariscs i les nostres carns de qualitat. </p>
                    </div>
                </div>
                <div class="ek-block">
                    <div class="ek-block-img floatLeft">
                        <img src="images/plates/flam.jpg" alt="" class="img_inner">
                        <img src="images/plates/shadow.png" alt="" class="img_shadow">  
                    </div>
                    <div class="ek-block-text floatLeft">
                        <h2>Postres</h2>
                        <p>Volem fer-te la vida més dolça, crema catalana, flam artesanal i els nostres pastissos son només alguns dels clàssics que reservem per tancar la nostra proposta gastronòmica.  </p>
                    </div>
                </div>
            </div>

        </section>
        <section class="ek-auto" id="lapTop2" data-step="2" data-kmxo="false" data-offset='true'>

            <div class="ek-grid3" style="position:relative;" >
                <h3 style="border-bottom: 1px solid #414141;"> LA NOSTRA HISTÒRIA... </h3>
                <div class="ek-block3" id="ek-output" >
                </div>
                <div class="ek-block4" id="ek-output-desc">
                    <span></span>
                </div>
                <!--SLIDES-->
                <div class="ek-block5 ek-selected" data-image="../images/history/any1935.jpg" >
                    <span>Inauburació La Bauma. Any 1935</span>
                </div>


                <div class="ek-block5" data-image="../images/history/any1938.jpg">
                    <span> Família Riera. Any 1938</span>
                </div>
                <div class="ek-block5" data-image="../images/history/any1951.jpg">
                    <span> Primera Barra. Any 1951</span>
                </div>
                <div class="ek-block5" data-image="../images/history/any1956.jpg">
                    <span> Família Riera. Any 1956</span>
                </div>
                <div class="ek-block5" data-image="../images/history/any1958.jpg">
                    <span> Ramón Riera. Any 1958</span>
                </div>
                <div class="ek-block5" data-image="../images/history/any1961.jpg">
                    <span> Família Riera. Any 1961</span>
                </div>
                <div class="ek-block5" data-image="../images/history/any1966.jpg">
                    <span> Segona reforma del local. Any 1966</span>
                </div>
                <div class="ek-block5" data-image="../images/history/terrassa.jpg">
                    <span> Terrassa de La Bauma</span>
                </div>

                <div class="ek-block5" style="margin-right: 0px !important;" data-image="../images/history/terrassa2.jpg">
                    <span> Terrassa de La Bauma</span>
                </div>


                <div class="ek-block2">
                    <p>El 1 d'abril del 1935, amb sol•licitud d'una llicència per obrir un cafè comença la història de La Bauma...</p>
                    <p>Després de molt lluitar, per fi, el somni d'en  Josep pren forma i es crea un dels locals més emblemàtics de Cardona.<br>
                        Família de tradició cafetera, avui 79 anys després el cognom Riera continua més viu que mai.</p> 

                    <p>De la Bauma en podríem dir moltes coses com, que continua la tradició de cafè de tota la vida, que tenen uns ocells tropicals que son famosos i la delícia de tota la canalla, que la Roser continua fent els Calamars més bons de tota la comarca.<br>
                        Amb el temps i un munt de reformes, avui, totalment equipat per la vida moderna, hi podeu gaudir de partits de futbol, de menús diaris, de menús de cap de setmana, d'un pop totalment diferent i com no d'una bona copa...</p>

                    <p>Passen els anys, però de veritat a la Bauma si hi ha una cosa que no ha canviat és el tracte que els ha caracteritzat, el tracte familiar.<br>
                        L'essència de taberna on vas a petar la xerrada i a  fer un cafè, el local on trobar-te amb els amics, tot gràcies a que en Josep va fer estimar el seu somni al seu fill en Ramón que és qui ara us rebrà com a bon cafeter amb un somriure a  la cara i us aconsellarà que prendre. </p>
                </div>
                <div class="ek-arrow" data-step="0">
                    <img style="margin-top: 11px; margin-left: 4px;" src="images/arrow2.png" width="10" >
                </div>
            </div>

        </section>

        <section class="ek-lapLarge" id="lapTop3" data-step="3" data-offset='false'>

            <div class="container">	


                <nav>

                </nav>

                <div id="cbp-fwslider" class="cbp-fwslider">
                    <ul>
                        <li style="height: 100%;"><img src="images/fotos/foto2.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto1.jpg" alt="img01"  style="width: 100%; "/></li>
                        
                        <li style="height: 100%;"><img src="images/fotos/foto3.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto4.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/back4.jpg" alt="img03"  style="width: 100%;  " /></li>
                        <li style="height: 100%;"><img src="images/fotos/foto5.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto6.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto7.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto8.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto9.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto10.jpg" alt="img02"  style="width: 100%;  "/></li>
                         <li style="height: 100%;"><img src="images/fotos/foto15.jpg" alt="img02"  style="width: 100%;  "/></li>
                        <li style="height: 100%;"><img src="images/fotos/foto11.jpg" alt="img02"  style="width: 100%;  "/></li>
                         <li style="height: 100%;"><img src="images/fotos/foto12.jpg" alt="img02"  style="width: 100%;  "/></li>
                          <li style="height: 100%;"><img src="images/fotos/foto13.jpg" alt="img02"  style="width: 100%;  "/></li>
                           <li style="height: 100%;"><img src="images/fotos/foto14.jpg" alt="img02"  style="width: 100%;  "/></li>
                    </ul>
                </div>
            </div>

        </section>

        <section class="ek-lapLarge" id="lapTop4" data-step="4" data-offset='false'>

            <div class="globalMap">
                <h2 style="text-align: center; color:white; margin-bottom: 30px;"> On ens trobem? </h2>
            <div class='ek-map' id='ek-map' data-map='true'></div>
            <p style="text-align: center; margin-top: 20px; color:white;">Pl. de la Fira, 19 - 08261 Cardona</p>
            </div>

            <div class="ek-contact">
                <div id="form-main">
                    <div id="form-div">
                        <span style="font-family: 'PT Sans', sans-serif; "> DEIXAN'S EL TEU MISSATGE! </span>
                        <form class="form" id="form1" onclick="return false;">

                            <p class="name">
                                <input name="name" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Nom" id="name" required />
                            </p>

                            <p class="email">
                                <input name="mail" type="text" class="validate[required,custom[email]] feedback-input" id="email" placeholder="E-mail" required />
                            </p>

                            <p class="text">
                                <textarea name="text" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="Missatge"></textarea>
                            </p>


                            <div class="submit">
                                <input type="submit" value="Enviar" id="button-blue"/>
                                <div class="ease"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                <!--<div class="ek-footer">

                </div>-->



        </section>
        <footer>
            <p style="text-align: center;">info@elmenutdelabauma.com | tel.938 691 002 | Pl. de la Fira, 19 - 08261 Cardona </p>
        </footer>

    </main>
</body>
</html>

